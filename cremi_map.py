import glob
import json
import regex

class CremiMap():
    __file_path = []

    '''
        Data format:
        dict room_map = {
            'room_name': [
                    ['pc0', {INFO}],
                    ['pc1', {INFO}]
                    ...
                ],
                'room_name': [
                    ['pc0', {INFO}],
                    ['pc1', {INFO}]
                    ...
                ],
            }

    '''
    __room_map = {}
    def __init__(self):
        pass

    def parse_file(self, file_path):
        f = open(file_path, 'r')
        for line in f:
            name = line.strip('\n')
            info = {}

            pc = [name, info]

            self.__room_map[file_path.split('/')[-1]].append(pc)


    '''
        Get informations associated with a computer (GPU, CPU, ...)
            Informations should be initialized at the first start.
    '''
    def get_infos(self, pc_name):
        name, info = self.get_computer(pc_name)
        return info


    def associate_infos(self, pc_list, infos):
        for key, room in self.__room_map.items():
            for index, value in enumerate(room):
                name = self.__room_map[key][index][0]
                for i, v in enumerate(pc_list):
                    if name == v:
                        self.__room_map[key][index][1] = infos

    '''
    Return the name and informations of the computer name given in parameter if it exist. Pair of None otherwise
    '''
    def get_computer(self, pc_name):
        for key, room in self.__room_map.items():
            for pc in room:
                name = pc[0]
                info = pc[1]

                if name == pc_name:
                    return [name, info]

        return [None, None]

    '''
     Return a list with all computers in a specific room
     INPUT:
        room: str
    '''
    def get_room(self, room):
        list_pc = []
        for key, pc in enumerate(self.__room_map[room]):
            list_pc.append(pc[0])
        return list_pc
        pass

    '''
     Return a list with all computers in a specific floor
     INPUT:
        floor: int
    '''
    def get_floor(self, floor):
        pc_name = []

        for key, room in self.__room_map.items():
            # The first letter in keys represent the floor
            # 001 : RDC
            # 101 : 1st floor
            # 201 : 2nd floor
            if key[0] == str(floor):
                # Append all computers name
                for pc in room:
                    pc_name.append(pc[0])

        return pc_name

    def get_all(self):
        pc_name = []

        for key, room in self.__room_map.items():
            for pc in room:
                pc_name.append(pc[0])

        return pc_name

    '''
        Return extra computer (jolicoeur, macgonagal...) /!\ jaguar not include
    '''
    def get_extra(self):
        pass

    '''
        Return a list of computers name to match with the list of filter given in parameter
    '''
    def get_with_filter(self, list_filter):
        list_matched = []

        # Iterate over information item of each computer
        for key, room in self.__room_map.items():
            for pc in room:
                name = pc[0]
                info = pc[1]
                for k_info, v_info in info.items():
                    for index, value in enumerate(v_info):

                        # Check all iterm in the list
                        for k_list, v_list in enumerate(list_filter):
                            if v_list == value:
                                list_matched.append(name)

        return list_matched

    def to_json(self, path):
        f = open(path, 'w')
        json.dump(self.__room_map, f, indent=2)

    def from_json(self, path):
        f = open(path, 'r')
        self.__room_map = json.load(f)

        return self

    def from_adhoc(self, path):
        # Load file
        for files in glob.iglob(path, recursive=True):
            # file = data/dir/file
            # get let filename
            name = files.split('/')[-1]

            self.__file_path.append(files)
            self.__room_map[name] = []

            self.parse_file(files)

        return self


if __name__ == '__main__':
    import socket
    import os

    # Run this script on every machines to complete the cremi_map.json file

    machine_name = socket.gethostname()
    file_name = 'cremi_map.json'

    room_map = CremiMap()
    if os.path.exists(file_name) == False:
        print('File doesn\'t exist')
        room_map.from_adhoc('data/*/*')
    else:
        room_map.from_json(file_name)

    infos = {
        'gpu': {
            'vendor': 'null',
            'model': ['null'],
            'ram': 'null'
        },
        'details' : infos_dict
    }

    room_map.associate_infos([machine_name], infos)
    room_map.to_json(file_name)
