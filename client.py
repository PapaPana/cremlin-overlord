from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random

from socket import socket, AF_INET, SOCK_STREAM

from config import CONFIG 
from thread import Thread

class Client:
    host = ''
    port = 0
    buffer_size = 2048

    _socket = None

    def __init__(self, config):
        self.host = config['host']
        self.port = config['port']
        self.buffer_size = config['buffer_size']
        
    def start(self, process_func=None, process_args=None):

        self._socket = socket(AF_INET, SOCK_STREAM)
        self._socket.connect((self.host, self.port))

        if process_func is not None:
            process_func(process_args)
    
    def _send(self, conn, data):
        conn.sendall(data.encode('utf-8'))
        pass

    def _recv(self, conn):
        data = b''
        while True:
            subdata = conn.recv(self.buffer_size)
            data += subdata

            if len(subdata) < self.buffer_size:
                break
        
        return data 
