from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random

import socket
import time
from threading import RLock

from config import CONFIG
from thread import Thread

import sys

class Server(Thread):
    host = ''
    port = 0
    buffer_size = 2048

    _socket = None
    _clients = {}

#    __threads = {}
#    __aes_keys = {}

    _lock = RLock()

    def __init__(self, config):
        self.host = config['host']
        self.port = config['port']
        self.buffer_size = config['buffer_size']

        Thread.__init__(self)

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.bind((self.host, self.port))
        self._socket.listen(10)

    def _recvall(self, conn):
        data = b''

        while True:
            subdata = conn.recv(self.buffer_size)

            data += subdata
            if len(subdata) < self.buffer_size:
                break
        return data
